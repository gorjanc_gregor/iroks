<?php
/**
 * Created by PhpStorm.
 * User: Gregor
 * Date: 14/01/2018
 * Time: 20:49
 */
include("database.php");
session_start();

if(isset($_GET["tema"]) ){
    $stmt = $db->prepare("SELECT vprasanje FROM forum WHERE id=?");
    $stmt->bind_param('i',$_GET["tema"]);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($vprasanje);
    while ($stmt->fetch()){
        ?>
        <div class="temavprasanje">
        <p>Podroben opis:</p>
        <h4><?php echo $vprasanje?></h4></div>
        <div class="temaodziv"><p class="odziv">Odzivi:</p></div>

        <?php
    }
    $stmta = $db->prepare("SELECT odgovori.odgovor, odgovori.cas, uporabnik.ime, uporabnik.Priimek  FROM odgovori Inner JOIN uporabnik on odgovori.uporabnik_id = uporabnik.id WHERE odgovori.forum_id=?");
    $stmta->bind_param('i',$_GET["tema"]);
    $stmta->execute();
    $stmta->store_result();
    $stmta->bind_result($odgovor, $cas, $ime, $priimek);
    while ($stmta->fetch()){
        ?><div class="temaodgovor">
            <p class="odgovortema">
                <?php echo $odgovor ?>
            </p>
        <p class="odgovortemalastnikcas">
            <?php echo $cas. ' '. $ime, ' '. $priimek ?>
        </p>
        </div>
        <?php

    }?>
    <input id="temahidden" type="hidden" value="<?php echo $_GET["tema"] ?>">
    <?php

} if(isset($_GET["poslji"])&&isset($_GET["kaj"])){
    $stmtv = $db->prepare("INSERT INTO `odgovori` (`odgovor`, `cas`, `uporabnik_id`,`forum_id`) VALUES (?, NOW(), ?, ?);");
    $stmtv->bind_param('sii', $_GET["kaj"], $_SESSION["id"] , $_GET["poslji"]);
    $stmtv->execute();



}