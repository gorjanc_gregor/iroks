<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 14. 01. 2018
 * Time: 15:19
 */


include('header.php');
if (isset($_SESSION["id"])) {

    if (isset($_POST["dodaj"])) {
        $stmt = $db->prepare("INSERT INTO forum(vprasanje,cas,naslov,uporabnik_id) VALUES (?, NOW(), ?, ?);");
        $stmt->bind_param('ssi', $_POST["vsebina"], $_POST["naslov"], $_SESSION["id"]);
        $stmt->execute();
    }

    $stmt = $db->prepare("SELECT forum.id,forum.naslov,forum.vprasanje,forum.cas,uporabnik.ime, uporabnik.priimek FROM forum INNER JOIN uporabnik ON forum.uporabnik_id=uporabnik.id ");

    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($id, $naslov, $vprasanje, $cas, $ime, $priimek);


    ?>
    <div class="forum">
        <?php
        while ($stmt->fetch()) {
            ?>
            <div class="teme temelevo" id="<?php echo $id ?>">


                <h3 class="tema" id="<?php echo $id ?>"><?php echo $naslov ?></h3>

                <p class="lastnikcas" id="<?php echo $id ?>"><?php echo $cas . ' ' . $ime . ' ' . $priimek ?></p>

            </div>
            <?php
        }

        ?>
        <div class="dodajtemo">
            <h2>Dodaj temo</h2>
            <form action="forum.php" method="post">


                Naslov:<br>
                <input class="dodajtemoinput" type="text" name="naslov" maxlength="45" required>
                <br>
                Opis:<br>
                <textarea class="dodajtemoinput" name="vsebina" maxlength="450" required></textarea><br>


                <input type="submit" name="dodaj" value="Dodaj" class="gumbtema">


            </form>
        </div>
    </div>

    <div class="forum">


        <div id="odgovori" >
           <div class="temavprasanje" > klikni na temo za prikaz</div>
        </div>

        <div id="vnosdiv" class="temaodgovori">
            <p>Dodaj odziv:</p>
            <textarea class="dodajtemoinput dodajodgovor" id="vnos" required maxlength="450"></textarea><br>
            <button id="gumbPoslji" class="gumbtema">Dodaj</button>

        </div>
    </div>
    <script>
        $(document).ready(function () {
            $(".teme").click(function (e) {


                $(".teme").css({
                    'color': 'black',
                    'background': 'white',
                    'border-style': 'solid',
                    'border-color': 'white'
                });

                $.get("tema.php?tema=" + e.target.id, function (data) {
                    $("#odgovori").html(data);
                    $("#vnosdiv").show();
                });


                $("#" + e.target.id).css({
                    'color': 'white',
                    'background': '#005EFF',
                    'border-style': 'solid',
                    'border-color': 'black'
                });


            });
        });
        $(document).ready(function () {

            $("#vnosdiv").hide();


        });

        $("#gumbPoslji").click(function (e) {

            $.get("tema.php?poslji=" + $("#temahidden").val() + "&kaj=" + $("#vnos").val(), function () {
                $.get("tema.php?tema=" + $("#temahidden").val(), function (data) {
                    $("#odgovori").html(data);
                    $("#vnosdiv").show();
                });
            });


            $("#vnos").val("")
        });
    </script>
<?php } else {
    ?><p>Za ogled foruma se prijavite</p>
    <?php
}
