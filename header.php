<?php
/**
 * Created by PhpStorm.
 * User: Gregor
 * Date: 13/01/2018
 * Time: 11:55
 */
session_start();
include('database.php'); ?>

<html>
<head>
    <link href="css.css" rel="stylesheet" type="text/css" media="screen"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>
<ul class="navigacija">
    <li class="navitems"><a href="index.php">Domača Stran</a></li>
    <li class="navitems"><a href="forum.php">Forum</a></li>
    <li class="navitems"><a href="klepetalnica.php">Klepetalnica</a></li>


    <?php if (isset($_SESSION["id"])) { ?>
        <li class="navitems"><a href="urediprofil.php">Uredi profil</a></li>
        <li >
            <span>Pozdravljen <?php echo ucfirst($_SESSION["me"]) ?></span>

            <form class="formInline" method="get" action="prijava.php">
                <input class="formInline" type="submit" name="odjavi" value="Odjava">

            </form></li>
    <?php } else {
        ?>
        <li class="navitems" id="prijava"><a  href="prijava.php">Prijava</a></li>
        <?php

    }?>

</ul>
