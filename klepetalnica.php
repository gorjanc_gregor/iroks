<?php
/**
 * Created by PhpStorm.
 * User: Gregor
 * Date: 13/01/2018
 * Time: 14:23
 */

include('header.php');

?>
    <head>
        <title>Klepetalnica</title>

    </head>


<?php if (isset($_SESSION["id"])) { ?>
    <div class="klepetalnica">
        <?php
        $stmt = $db->prepare("SELECT id, ime, priimek FROM uporabnik");
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($id, $ime, $priimek);

        while ($stmt->fetch()) {
            ?>
            <div class="oseba" id="<?php echo $id ?>">
                <?php
                echo $ime . ' ' . $priimek;

                ?>

            </div>


            <?php
        }


        ?>

    </div>
    <div class="klepetalnica chat">
        <div id="pogovor">
            Klikni na osebo za pogovor
        </div>


        <div id="vnosdiv" class="vnosBesedila">

            <input type="text" id="vnos" maxlength="450" required>
            <button id="gumbPoslji">Pošlji</button>

        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(".oseba").click(function (e) {


                $(".oseba").css({
                    'color': 'black',
                    'background': 'white',
                    'border-style': 'solid',
                    'border-color': 'white'
                });

                $.get("chat.php?&posiljatelj=" + e.target.id, function (data) {
                    $("#pogovor").html(data);
                    $("#vnosdiv").show();
                });


                $("#" + e.target.id).css({
                    'color': 'white',
                    'background': '#005EFF',
                    'border-style': 'solid',
                    'border-color': 'black'
                });


                setInterval(function () {
                    $.get("chat.php?&posiljatelj=" + $("#komu").val(), function (data) {
                        $("#pogovor").html(data);
                        $("#vnosdiv").show();
                    });
                }, 1000);
            });
        });
        $("#gumbPoslji").click(function (e) {

            $.get("chat.php?poslji=" + $("#komu").val() + "&kaj=" + $("#vnos").val(), function () {
                $.get("chat.php?&posiljatelj=" + $("#komu").val(), function (data) {
                    $("#pogovor").html(data);
                    $("#vnosdiv").show();
                });
            });


            $("#vnos").val("")
        });

        $(document).ready(function () {
            $("#<?php echo $_SESSION["id"] ?>").hide();
            $("#vnosdiv").hide();


        });

    </script>

<?php } else {
    ?>
    Za uporabo klepetalnice se prijavite!


    <?php


}

?>