<?php
/**
 * Created by PhpStorm.
 * User: Gregor
 * Date: 13/01/2018
 * Time: 14:57
 */

include("database.php");
session_start();

if (isset($_GET["posiljatelj"])) {
    $stmt = $db->prepare("SELECT * FROM chat WHERE (posiljatelj=? AND prejemnik=?) OR (prejemnik=? AND posiljatelj=?) ");
    $stmt->bind_param('iiii', $_SESSION["id"], $_GET["posiljatelj"], $_SESSION["id"], $_GET["posiljatelj"]);
    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($id, $text, $datum_cas, $posiljatelj, $prejemnik);

    while ($stmt->fetch()) {
        if ($_SESSION["id"] == $posiljatelj) {
            ?>
            <div class="chatdiv">
                <div class="jazdiv">
                    <p class="jaz break"><?php echo $text ?></p>
                    <p class="casjaz"><?php echo $datum_cas ?></p>
                </div>

            </div>
            <?php


        } else {
            ?>
            <div class="chatdiv">
                <div class="drugidiv">
                    <p class="drugi break"><?php echo $text ?></p>
                    <p class="casdrugi"><?php echo $datum_cas ?></p>
                </div>

            </div>

            <?php

        }


    } ?>
    <input id="komu" type="hidden" value="<?php echo $_GET["posiljatelj"] ?>">


    <?php


}
if (isset($_GET["poslji"]) && isset($_GET["kaj"])) {
    $stmtv = $db->prepare("INSERT INTO `chat` (`text`, `cas`, `posiljatelj`,`prejemnik`) VALUES (?, NOW(), ?, ?);");
    $stmtv->bind_param('sii', $_GET["kaj"], $_SESSION["id"] , $_GET["poslji"]);
    $stmtv->execute();

}


?>

