-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: iroks
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(450) DEFAULT NULL,
  `cas` datetime DEFAULT NULL,
  `posiljatelj` int(11) NOT NULL,
  `prejemnik` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_chat_uporabnik1_idx` (`posiljatelj`),
  KEY `fk_chat_uporabnik2_idx` (`prejemnik`),
  CONSTRAINT `fk_chat_uporabnik1` FOREIGN KEY (`posiljatelj`) REFERENCES `uporabnik` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_chat_uporabnik2` FOREIGN KEY (`prejemnik`) REFERENCES `uporabnik` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat`
--

LOCK TABLES `chat` WRITE;
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
INSERT INTO `chat` VALUES (99,'pls no burn','2018-01-14 16:15:54',6,5),(100,'Where are the emails?','2018-01-14 16:17:02',5,6),(101,'hmm','2018-01-14 16:18:23',5,6),(102,'Hola!','2018-01-14 16:33:14',8,5),(103,'...','2018-01-14 16:33:20',8,6),(104,'Jeb!','2018-01-14 16:33:52',5,8),(106,'y u no answer?','2018-01-14 22:10:16',6,8),(109,'jail','2018-01-15 14:26:03',5,6);
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum`
--

DROP TABLE IF EXISTS `forum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vprasanje` varchar(45) DEFAULT NULL,
  `cas` datetime DEFAULT NULL,
  `naslov` varchar(45) DEFAULT NULL,
  `uporabnik_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_forum_uporabnik1_idx` (`uporabnik_id`),
  CONSTRAINT `fk_forum_uporabnik1` FOREIGN KEY (`uporabnik_id`) REFERENCES `uporabnik` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum`
--

LOCK TABLES `forum` WRITE;
/*!40000 ALTER TABLE `forum` DISABLE KEYS */;
INSERT INTO `forum` VALUES (2,'pls halp','2018-01-14 20:10:18','How to become a president?',6),(3,'pls respond','2018-01-14 21:30:30','how to delete your emails?',6);
/*!40000 ALTER TABLE `forum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `odgovori`
--

DROP TABLE IF EXISTS `odgovori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `odgovori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `odgovor` varchar(450) DEFAULT NULL,
  `cas` datetime DEFAULT NULL,
  `forum_id` int(11) NOT NULL,
  `uporabnik_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_odgovori_forum_idx` (`forum_id`),
  KEY `fk_odgovori_uporabnik1_idx` (`uporabnik_id`),
  CONSTRAINT `fk_odgovori_forum` FOREIGN KEY (`forum_id`) REFERENCES `forum` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_odgovori_uporabnik1` FOREIGN KEY (`uporabnik_id`) REFERENCES `uporabnik` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `odgovori`
--

LOCK TABLES `odgovori` WRITE;
/*!40000 ALTER TABLE `odgovori` DISABLE KEYS */;
INSERT INTO `odgovori` VALUES (1,'easy','2018-01-14 21:04:07',2,5),(2,'is no','2018-01-14 21:54:10',2,6),(3,'pls','2018-01-14 21:55:24',2,6);
/*!40000 ALTER TABLE `odgovori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uporabnik`
--

DROP TABLE IF EXISTS `uporabnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uporabnik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Ime` varchar(45) DEFAULT NULL,
  `geslo` varchar(45) DEFAULT NULL,
  `uporabniskoime` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `Priimek` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uporabnik`
--

LOCK TABLES `uporabnik` WRITE;
/*!40000 ALTER TABLE `uporabnik` DISABLE KEYS */;
INSERT INTO `uporabnik` VALUES (5,'donald','e358efa489f58062f10dd7316b65649e','d','gaga@gmail.com','trump'),(6,'hillary','2510c39011c5be704182423e3a695e91','h','hillary@clinton.com','clinton'),(8,'jeb','dbbc546e357040b4a83a65f387d5c06a','jeb','jeb@bush.com','bush');
/*!40000 ALTER TABLE `uporabnik` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-15 15:58:35
